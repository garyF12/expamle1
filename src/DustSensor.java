
public class DustSensor extends Sensor implements SensorI{

	public boolean turnOn() {
		setState(true);
		return getState();
	}

	public double sensorValue() {
		setValue(Math.random());
		return getValue();
	}

	public boolean turnOff() {
		setState(false);
		return getState();
	}



}
