import java.util.Scanner;
public class Main {
	
	public static void main(String[] args) {
		Scanner scann = new Scanner(System.in);
		Scanner op = new Scanner(System.in);
		boolean back = true;
		TemperatureSensor ts = new TemperatureSensor();
		DustSensor ds = new DustSensor();
		
		/*int option;
		System.out.println("Trun On Temperature Sensor");
		System.out.println("1.Yes");
		System.out.println("2.No");
		option = op.nextInt();
		if(option == 1){
			System.out.println(ts.turnOn());
			System.out.println(ts.sensorValue());
		}*/
		
		while(back == true ){
			System.out.println("Choose an option");
			System.out.println("1.Temperature Sensor");
			System.out.println("2.Dust Sensor");
			System.out.println("6. Back");
		
			int option = op.nextInt();
			switch(option){
				case 1:
					System.out.println("1.TEMPERATURE SENSOR ON");
					System.out.println(ts.turnOn());
					System.out.println("1.Sensor data:");
					for(int i=0; i<10; i++){
						System.out.println(ts.sensorValue());
					}
					ts.turnOff();
					break;
				case 2:
					System.out.println("1.DUST SENSOR ON");
					System.out.println(ds.turnOn());
					System.out.println("1.Sensor data:");
					for(int i=0; i<10; i++){
						System.out.println(ds.sensorValue());
					}
					ds.turnOff();
					break;
				case 3:
					back = false;
					break;
			}
		}
	}

}
